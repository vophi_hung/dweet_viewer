var express = require('express');
var router = express.Router();
var Dweet = requireFromRoot('models/dweet_data')
var fs = require("fs")
var path = require("path")
var moment = require("moment")
var moment_timezone = require("moment-timezone")

router.get('/', function(req, res, next) {
  res.render("frontpage/index");
});

router.post('/export', function(req, res, next) {
  var cri = {
    $and: [{
      dateCreated: {
        $gt: req.body.from || 0
      }
    }, {
      dateCreated: {
        $lt: req.body.to || 0
      }
    }]
  }

  Dweet.find(cri)
    .then((data) => {
      var exportDt = ["date, time, name, sensorValue, sensorValue2, sensorValue3, sensorValue4, sensorValue5"]
      _.each(data, (dt) => {

        var itTime = moment(dt.dateCreated).tz("Asia/Ho_Chi_Minh")
        exportDt.push([
          itTime.format('DD-MM-YYYY'),
          itTime.format('HH:mm:ss'),
          dt.name,
          dt.sensorValue,
          dt.sensorValue2,
          dt.sensorValue3,
          dt.sensorValue4,
           dt.sensorValue5
        ].join(","))
      })

      var current = moment().tz("Asia/Ho_Chi_Minh").format('DD-MM-YYYY')// chinh ten file theo dinh dang HH:mm:ss
      var exportText = exportDt.join("\n");
      //write to file 
      fs.writeFile(path.join(__dirname, "..", "public", current + ".csv"), exportText, function(err) {
        if (err) {
          return res.json(err);
        }

        res.json(current + ".csv")
      });
      
    })

});


router.get('/data', function(req, res, next) {
  var params = _.pick(req.query, "sensorValue", "sensorValue2", "sensorValue3", "sensorValue4", "sensorValue5","name");
  params.name = params.name || "sensor1"
  var dweetObj = new Dweet({
    name: 'sensor1',
    sensorValue: params.sensorValue,
    sensorValue2: params.sensorValue2,
    sensorValue3: params.sensorValue3,
    sensorValue4: params.sensorValue4,
    sensorValue5: params.sensorValue5
  });
  dweetObj.name = params.name;
  dweetObj.save();
  io.emit('new_data', dweetObj.toObject())


  res.send("ok")
})

router.get('/getData', function(req, res, next) {
  Dweet.find()
  .limit(10)
  .sort({dateCreated: -1})
  .then(function(data){
    res.send(data)
  })
})

module.exports = router;