var express = require('express');
var router = express.Router();

var CategoryCtl = requireFromRoot('controllers/category')

router.get('/', CategoryCtl.getList);
router.post('/', CategoryCtl.create);
router.put('/:id', CategoryCtl.edit);
router.delete('/:id', CategoryCtl.delete);

module.exports = router;