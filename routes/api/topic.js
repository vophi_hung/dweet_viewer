var express = require('express');
var router = express.Router();

var TopicCtl = requireFromRoot('controllers/topic')

router.get('/', TopicCtl.getList);
router.post('/', TopicCtl.create);
router.put('/:id', TopicCtl.edit);
router.delete('/:id', TopicCtl.delete);

module.exports = router;