require('./global');
var application_root = __dirname,
    express = require("express"),
    http = require('http'),
    path = require("path"),
    fs = require("fs"),
    mongoose = require('mongoose'),
    argv = require('optimist').argv
    
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "development";
}



/* CONNECT TO DATABASE HERE */
mongoose.connect('mongodb://localhost/' + config.database);

var app = require('./express');
var server = http.createServer(app);


server.listen(config.port, function(){
	console.log("listen from port " + config.port);
});


var Dweet = requireFromRoot('models/dweet_data')

global.io = require('socket.io')(server);

io.on('connection', function(socket) {
	console.log('Browser connected.');
	socket.emit('message', 'Connected to server at ' + new Date().toString() + '\n');
	socket.on('message', function(data) {
		console.log('from client: ', data);
		
	});
});

// dweetio.listen_for(config.dweet_name, function(dweet){
// 	var dweetObj = new Dweet(dweet.content);
// 	dweetObj.name = config.dweet_name;
// 	dweetObj.save();
//     io.emit('new_data', dweet)
// });
// 
// 

var WebSocketServer = require('websocket').server;

 wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed. 
  return true;
}
 
wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      // Make sure we only accept requests from an allowed origin 
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    var connection = request.accept();
    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            var dweetObj = new Dweet();
            console.log(message.utf8Data)
            var data = message.utf8Data.split(':');

            dweetObj.sensorValue = parseFloat(data[1])
            dweetObj.sensorValue2 = parseFloat(data[2])
            dweetObj.sensorValue3 = parseFloat(data[3])
            dweetObj.sensorValue4 = parseFloat(data[4])
            dweetObj.sensorValue5 = parseFloat(data[5])
            dweetObj.name = data[0];
            dweetObj.save();
            dweetObj.dateCreated = (new Date()).getTime()
            io.emit('new_data', dweetObj.toObject())
            
            //connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
