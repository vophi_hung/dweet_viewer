var schema = new mongoose.Schema({
  dateCreated: Number,
  timestamp: Number
});
schema.pre("save", function(next){
 this.dateCreated = (new Date()).getTime();
 this.timestamp = this.dateCreated;
 next();
});

module.exports = schema;