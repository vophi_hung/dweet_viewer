var crypto = require('crypto')
  , Representatives = requireFromRoot('models/representative')
  , aws = requireFromRoot("controllers/aws")
  , User_session = requireFromRoot('models/user_session')
  , twitterAPI = require('node-twitter-api')
  , twitter = new twitterAPI({
    consumerKey: 'lBQMkB5xQPgfNP7v6wbBpGrLU',
    consumerSecret: 'qwRmhq8eVV2gNLCP8Zt0w0bcJV1fAoBfBEBQTHtkTp0DLriVmg',
    callback: 'http://sivy.io:6789/'
  })

var representatives_officesModel = BaseModel.extend({
  type: String, 
  fax: String,
  postal : String,
  tel : String
});

var schema = BaseModel.extend({
  avatar : String,
  type : {
    type: String, 
    default: "USER",
    enum: ["USER", "REPRESENTATIVE", "SUPPORTER"]
  }, 
  idRepresentative : String, //if user type = REPRESENTATIVE
  postalCode : String,
  firstName: String,
  lastName: String,
  name: String,
  email: String,
  phone: String,
  password: String,
  age: Number,
  gender: {type: String, default: ""}, //1: M, F, O
  address: String,
  city: String,
  province: {
    type: String, 
    default: "Canada"
  },
  country: String,
  //Representative area
  elected_office: String,
  elected_level: {
    type: String, 
    enum: ["MUNICIPAL", "PROVINCIAL", "FEDERAL"]
  },
  url : String,
  chamber : String,
  offices : [representatives_officesModel],
  representative_set_name : String,
  party_name : String,
  description: String,
  district_name: String,
  domain : String,
  tel : String,
  personal_url : String,
  marker_image: String,
  extra : {
    facebook: String,
    twitter: String
  },
  numberView: {type: Number, default: 0},
  coordinate: {
    longitude: Number,
    latitude: Number
  },
  representativeStatus : {
    type : String , 
    enum: ["APPROVE","NOT_APPROVE"], 
    default: "NOT_APPROVE" 
  },
  //end
  socialType : {type: String, default: "NORMAL"},
  socialId : String,
  status: {
    type: String,
    enum : ["UNACTIVATED", "ACTIVATED", "HASINFORMATION", "MISSINGINFORMATION"] 
  },
  activeCode : String
});
schema.statics.getUserConversation = function(params,options) {
  var self = this;
  var promise = promiseAdapter.defer();
  self.findOne({_id: params.idUser})
    .then(function(user){
      if(user) {
        var dataUser = {
          _id : user._id ,
          name : user.name ,
          avatar: user.avatar || "",
          type: user.type
        }
        self.findOne({_id: params.idRepresentative})
          .then(function(representative){
            if(!representative) {
              promise.reject(null);
            }
            var dataRepresentative = {
              _id : representative._id ,
              name : representative.name ,
              avatar: representative.avatar || "",
              type: representative.type
            }
            data = {
              user : dataUser,
              representative : dataRepresentative
            }
            promise.resolve(data);
          });
      } else {
        promise.reject(null);
      }
    },function(user){
      promise.reject(null);
    })
  return promise.promise;
}
schema.statics.createRepresentative = function(params,options) {
  var self = this;
  return self.findOne({email: params.email})
    .then(function(item){
      if(!item) {
        var representative = self({
          type : "REPRESENTATIVE",
          first_name : params.firstName,
          last_name: params.lastName,
          name : params.firstName + params.lastName,
          elected_office: params.electedOffice, // level govement
          party_name : params.partyName,
          address : params.address,
          email: params.email,
          tel : params.tel,
          city : params.city,
          province : params.province,
          postalCode : params.postalCode,
          numberView: 0
        });
        return representative.save()
          .then(function(item){
            return item;
          });
      } else {
        return null;
      }
    })
}
function getTokenAndShowData(user, options){
  //user = user.toObject();
  var promise = promiseAdapter.defer();
  var currentTimestamp = (new Date()).getTime();
  md5session = crypto.createHash('md5');
  user.postalCode = user.postalCode.toUpperCase().trim().split(" ").join("");
  user.save()
  .then(function(dt){
    user = user.toObject();

    delete user.password;
    delete user.activeCode;
    delete user.active;
    md5session.update(user._id + currentTimestamp);
    hexSession = md5session.digest("hex");
    User_session.getSession({
    	idUser : user._id,
      sessionId: hexSession,
      sessionTime:currentTimestamp,
      idDevice: options.idDevice,
      pushToken: options.pushToken,
      os: options.os
    })
    .then(function(session){
    	user.sessionId = session.sessionId;
      user.sessionTime = currentTimestamp;  
      promise.resolve({
	      code: responseCode.SUCCESS,
	      description : "Success",
	      response: user
	    });
    }, function(err){
      promise.reject({
        code: responseCode.FAIL,
        description : err,
        response: null
      })
    })
  }, function(err){
  	promise.reject({
      code: responseCode.FAIL,
      description : err,
      response: null
    });
  });  
  return promise.promise;
}

schema.statics.loginNormal = function(data, options){
  var promise = promiseAdapter.defer();
	var self = this;
	self.findOne({email: data.email})
	.then(function(user){
		if (user == null) {
			promise.reject({
	      code: responseCode.DATA_NOT_EXISTS,
	      description : "Email does not exists",
	      response: null
	    });
		} else {
			var md5 = crypto.createHash('md5');
			md5.update(data.password);
			hexPass = md5.digest("hex");
			if (hexPass != user.password) {
				promise.reject({
		      code: responseCode.UNAUTHORIZED,
		      description : "The email address or password is incorrect",
		      response: null
		    });
			} else {
				if (user.status == "UNACTIVATED") {
					promise.reject({
			      code: responseCode.OBJECT_NOT_ACTIVE,
			      description : "User is unactivated",
			      response: null
			    });
				} else {
					user.postalCode = data.postalCode.toUpperCase().trim().split(" ").join("");
					user.save()
					.then(function(user) {
						promise.resolve(user);
					});	
				}
			}
		}
	}, function(err){
    promise.reject({
      code: responseCode.FAIL,
      description : err,
      response: null
    });
  });

	return promise.promise
	.then(function(user){
		return getTokenAndShowData(user, options);
	}, function(err){
		return err;
	})
}

schema.statics.loginFacebook = function(params, options){
  var promise = promiseAdapter.defer();
  var self = this;
  try{
    requestData("https://graph.facebook.com/me?access_token="+params.accessToken+"&fields=email,name,first_name,last_name,gender", "GET", null, null, "https")
    .then(function(resp){
      var dataToken = resp;
      self.findItem({socialId : dataToken.id, socialType: "FACEBOOK"})
      .then(function(data){
        if (data == null) {//chưa có
          var user = new self({
            firstName: dataToken.first_name,
            lastName: dataToken.last_name,
            name: dataToken.name,
              email: dataToken.email,
              socialType : "FACEBOOK",
              socialId : dataToken.id,
              avatar : "https://graph.facebook.com/"+dataToken.id+"/picture?type=large",
              type: "USER"
          });
          user.save()
          .then(function(user){
            user.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
            user.save()
            .then(function(user){
              promise.resolve(user)
            });
          });
        } else {
          
          data.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
          
          data.save().then(function(user){
            promise.resolve(user)
          });
        }
      });
    }, function(err){
      promise.reject({
        code: responseCode.FAIL,
        description : err,
        response: null
      });
    })
  }catch(e){
    promise.reject(e)  
  }

  return promise.promise
  .then(function(user){
    return getTokenAndShowData(user, options);
  })
}

schema.statics.loginGoogle = function(params, options){
  var promise = promiseAdapter.defer();
  var self = this;

  requestData("https://www.googleapis.com/plus/v1/people/me?access_token="+params.accessToken, "GET", null, null, "https")
  .then(function(resp){
    var data = resp;
    self.findOne({socialId : data.id, socialType: "GOOGLE"})
    .then(function(rdata){
      if (rdata == null) {//chưa có
        var user = new self({
          firstName: data.name.givenName,
          lastName: data.name.familyName,
          name: data.displayName,
            email: data.email,
            socialType : "GOOGLE",
            socialId : data.id,
            avatar : data.image.url,
            type: "USER"
        });
        user.save()
        .then(function(user){
          user.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
          user.save()
          .then(function(user){
            promise.resolve(user)
          });
        }, function(err){
          promise.reject({
            code: responseCode.FAIL,
            description : err,
            response: null
          });
        });
      } else {
        console.log('r3');
        console.log(params.postalCode)
        rdata.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
        rdata.save()
        .then(function(user){
          console.log('r4')
          promise.resolve(user)
        }, function(err){
          promise.reject({
            code: responseCode.FAIL,
            description : err,
            response: null
          });
        });
      }
    }, function(err){
      promise.reject({
        code: responseCode.FAIL,
        description : err,
        response: null
      });
    });
  }, function(err){
    promise.reject({
      code: responseCode.FAIL,
      description : err,
      response: null
    });
  });
   
  return promise.promise
  .then(function(user){
    return getTokenAndShowData(user, options);
  }) 
}

schema.statics.loginTwitter = function(params, options){
  var promise = promiseAdapter.defer();
  var self = this;
  twitter.verifyCredentials(params.accessToken, params.password, 
    function(error, data, response) {
    if (error) {
        outData(apiconst.FAIL, error, "",res);
    } else {
      self.findOne({socialId : data.id, socialType: "TWITTER"})
      .then( function(err, rdata){      
        if (rdata == null) {//chưa có
          var user = new self({
            socialType : "TWITTER",
              socialId : data.id,
              name : data.screen_name,
              avatar : data.profile_image_url,
              type: "USER"
          });
          var listName = data.screen_name.split(" ");
          user.firstName = listName[0];
          user.lastName = "";
          for(i=1; i< listName.length; i++){
            user.lastName += " " + listName[i];
          } 

          user.lastName = user.lastName.trim();
          user.save()
          .then(function(err,user){
            user.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
            user.save()
            .then(function(user){
              promise.resolve(user)
            }, function(err){
              promise.reject({
                code: responseCode.FAIL,
                description : err,
                response: null
              });
            });
          }, function(err){
            promise.reject({
              code: responseCode.FAIL,
              description : err,
              response: null
            });
          });
        } else {
          rdata.postalCode = params.postalCode.toUpperCase().trim().split(" ").join("");
          rdata.save().then(function(user){
            promise.resolve(user)
          }, function(err){
            promise.reject({
              code: responseCode.FAIL,
              description : err,
              response: null
            });
          });
        }
      }, function(err){
        promise.reject({
          code: responseCode.FAIL,
          description : err,
          response: null
        });
      });      
    }
  });
  return promise.promise
  .then(function(user){
    return getTokenAndShowData(user, options);
  }) 
}

schema.statics.register = function(params, options){
  var self = this;
  var promise = promiseAdapter.defer();
  var isSendActivateMail = false;
  self.findOne({email: params.email})
  .then(function(user){
    if (user) {
      promise.reject({
          code: responseCode.DATA_EXSISTS,
          description : "Email has been taken by another user",
          response: null
      });
    } else {
      md5Code = crypto.createHash('md5');

      md5Code.update(makeid(10) + (new Date()).getTime());
      var loginVerify = md5Code.digest("hex");

      var md5 = crypto.createHash('md5');
      md5.update(params.password);
      var hexPass = md5.digest("hex");
  
      var user = new self({
        email : params.email,
        password : hexPass,
        firstName : params.firstName,
        lastName : params.lastName,
        name : params.firstName + " " + params.lastName,
        postalCode : params.postalCode,
        activeCode: loginVerify,
        type: params.type
      });
      if (params.type.toUpperCase() == "CANDIDATE") {
        user.status = "UNACTIVATED";
        //getCandidate
        Representatives.findOne({email : params.email})
        .then(function(cddata){
          if (cddata == null) {
            //getDomain
            var listdm = params.email.split('@');
            if (listdm.length == 2) {
              //filter domain
              if (listdm[1] == "google.com" || listdm[1] == "yahoo.com")  {
                promise.reject({
                  code : responseCode.FAIL,
                  description : "You cant register as a candidate or supporter for candidate with this email!",
                  response: null
                });
              } else {
                Representatives.findOne({domain : listdm[1]})
                .then(function(cddata){
                  if (cddata == null) {
                    promise.reject({
                      code : responseCode.FAIL,
                      description : "Email or domain does not exists on server!",
                      response: null
                    });  
                  } else {
                    user.type = "SUPPPORTER";
                    user.idCandidate = cddata._id;
                    user.save()
                    .then(function(user){
                      var content = {};
                      content.candidate = cddata;
                      content.candidate.first_name = params.firstName;
                      content.candidate.last_name = params.lastName;
                      content.email = params.email;
                      content.password = params.password;
                      content.cofirmationLink = "http://www.sivy.io/verify/"+ user._id +"/" + user.activeCode;
                      isSendActivateMail = true;
                      promise.resolve(content);
                      /*aws.sendRegisterMail("contact@sivy.io", "<Sivy> Please activate your new account", content)
                      .then(function (err,data){
                          if (err) {
                              outData(apiconst.FAIL, err, null,res);
                          }
                          else
                            outData(apiconst.SUCCESS, "Success", null,res);
                           
                      });  */
                    }, function(err){
                      promise.reject({
                        code : responseCode.FAIL,
                        description : "Email or domain does not exists on server!",
                        response: null
                      });  
                    });
                  }                  
                }, function(err){
                  promise.reject({
                    code : responseCode.FAIL,
                    description : err,
                    response: null
                  });
                });
              }
            }
            else{
              promise.reject({
                code: responseCode.FAIL,
                description : "Email doesn't correct",
                response: null
              });
            }     
          } else {
            user.save()
            .then(function(user){
              var content = {};
              content.candidate = cddata;
              content.email = params.email;
              content.password = params.password;
              content.cofirmationLink = "http://www.sivy.io/verify/"+ user._id +"/" + user.activeCode;
              promise.resolve(content);
              isSendActivateMail = true;
            }, function(err){
               promise.reject({
                code: responseCode.FAIL,
                description : err,
                response: null
              });
            });
          }       
        }, function(err){
          promise.reject({
            code: responseCode.FAIL,
            description : err,
            response: null
          });
        });
      } else {
        user.status = "ACTIVATED";
        user.save()
        .then(function(user){
            promise.resolve(user);
        }, function(err){
          return promise.reject({
            code: responseCode.FAIL,
            description: err,
            response: null
          });
        });
      }
    }
  },function(err){
    promise.reject(err);
  });
	
	if (isSendActivateMail) {
		return promise.promise
	  	.then(function(data){
	      return aws.sendRegisterMail("contact@sivy.io", "<Sivy> Please activate your new account", content)
	      .then(function(data){
	        return {
	          code: responseCode.SUCCESS,
	          description: "Success",
	          response: null
	        };
	      }, function(err){
	        return {
	          code: responseCode.FAIL,
	          description: err,
	          response: null
	        };
	      });     
  		});
	} else {
      return promise.promise.then(function(user){
      	return getTokenAndShowData(user, options);
      });
    }
}


schema.statics.create = function(params,options) {
  var self = this;
  var representative = self({
    first_name : params.firstName,
    last_name: params.lastName,
    name : params.firstName + params.lastName,
    elected_office: params.electedOffice, // level govement
    party_name : params.partyName,
    address : params.address,
    email: params.email,
    tel : params.tel,
    city : params.city,
    province : params.province,
    postalCode : params.postalCode,
    numberView: 0
  });
  return representative.save()
    .then(function(item){
      return item;
    });
}

schema.methods.getIssue = function(){
  var promise = promiseAdapter.defer();
  Issue.find({idRepresentative: this._id})
  .then(function(listIssue){
    var tasks = [];
    _.each(listIssue, function(issue){
      tasks.push(issue.getSocial({idUser: issue.id})
        .then(function(social){
          issue = issue.toObject();
          issue.user = social.user;
          issue.social = social.social;
          issue.idUser = undefined;
          return issue;
        }))
    })
    promiseAdapter.all(tasks)
    .then(function(issues){
      promise.resolve(issues);
    })
  }, function(err){
    promise.reject({
      code: responseCode.FAIL,
      description : err,
      response: null
    });
  });

  return promise.promise;
}

schema.methods.postIssue = function(params){
  var promise = promiseAdapter.defer();
  var issueObj = new Issue(params);
  issueObj.save()
  .then(function(issue){
    var tIssue = issue.toObject();
    issue.getSocial({idUser: issue.id})
    .then(function(social){
      tIssue.user = social.user;
      tIssue.social = social.social;
      promise.resolve(tIssue);
    })
  }, function(err){
    promise.reject({
      code: responseCode.FAIL,
      description : err,
      response: null
    });
  });

  return promise.promise;
}
module.exports = User = mongoose.model('user', schema);