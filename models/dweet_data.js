var dweetModel = BaseModel.extend({
  name: String,
  sensorValue: Number,
  sensorValue2: Number,
  sensorValue3: Number,
  sensorValue4: Number,
  sensorValue5: Number,
});

module.exports = Dweet = mongoose.model('dweet_data', dweetModel);