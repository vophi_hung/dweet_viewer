var categoriesModel = BaseModel.extend({
  name: String, 
  type: {
    type: String, 
    default: "BLOG",
    enum: ["BLOG", "BOOK"]
  },
  color: String,
  image : String,
  background: String
});

module.exports = Category = mongoose.model('category', categoriesModel);