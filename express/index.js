var express = require('express')
	, path = require('path')
	, logger = require('morgan')
	, bodyParser = require('body-parser')
  , favicon = require('serve-favicon')
  ,	engine = require('ejs-mate')
  

var app = express();
app.engine('ejs', engine);

app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use('/bower_components',  express.static(path.join(__dirname, '..', 'bower_components')));
/*import routes*/
app.set('views', path.join(__dirname, '..', 'views'));
app.set('view engine', 'ejs'); // so you can render('index')

console.time("Loading route index");
var indexRouter = requireFromRoot("routes/index");
app.use("/",indexRouter);
console.timeEnd("Loading route index");
var versions = config.version || ["v1.0"];
var type =  config.type || "dev";


//Response helper 
var response_helpers = requireFromRoot("libs/response_helpers")
app.use(response_helpers);

/*end load*/
routes = [];
_.each(versions, function(version){
	_.each(routes, function(route){
	  console.time("Loading route " + route);
	  contentRouter = requireFromRoot("routes/api/"  + route);
	  app.use("/api/" +  route,contentRouter);
	  console.timeEnd("Loading route " + route);
	});
});

/*end load routes*/
module.exports = app;