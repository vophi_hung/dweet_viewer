var redis = require("redis"),
    client = redis.createClient();

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });

client.on("error", function (err) {
    console.log("Error " + err);
});


client.on('connect', function() {
    console.log('connected');
});

exports.setData = function(params){
	var promise = promiseAdapter.defer();
	var params = _.pick(params, "key", "data");
	client.set(params.key, params.data, function(err, reply) {
	  if (err) {
	  	promise.reject(err);
	  } else {
	  	promise.resolve(reply)
	  }
	});

	return promise.promise;
}

exports.getData = function(params){
	var promise = promiseAdapter.defer();
	var params = _.pick(params, "key");
	client.get(params.key, function(err, reply) {
	  if (err) {
	  	promise.reject(err);
	  } else {
	  	promise.resolve(reply)
	  }
	});

	return promise.promise;
}