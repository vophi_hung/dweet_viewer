module.exports = function(req, res, next) {
  res.jsonData = (data) => {
    data = {
      code: data.code,
      description: data.description,
      response: data.response
    };
    res.json(data);
  }

  res.jsonError = (data) => {
    data = {
      code: data.code,
      description: data.description,
      response: data.response
    };
    res
    .status(400)
    .json(data);
  }

  next();
}