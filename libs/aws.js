var aws = require('aws-sdk')
  , fs = require('fs')
  , http = require('http')
  , https = require('https')
  , graphic = requireFromRoot("libs/graphic")
  , request = require('request')


aws.config.update({
	accessKeyId: config.aws.accessKeyId, 
	secretAccessKey: config.aws.secretAccessKey
});

var s3 = new aws.S3();

// Email config
var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var transporter = nodemailer.createTransport(ses({
    accessKeyId: config.ses.accessKeyId,
    secretAccessKey: config.ses.secretAccessKey
}));

//end email config
//
//

var upload = function(params){
	var promise = promiseAdapter.defer();	
	var fileStream = fs.createReadStream(params.name);
	var postParams = {
		Bucket: params.bucket, 
		Key: params.key, 
		Body: fileStream,
	    ContentType: params.type || "image/png",
	    ACL: 'public-read'
	}

	s3.putObject(postParams, function(err, data) {
	  	if (err) {
	  		promise.reject(err);
	  	} else {
	  		promise.resolve(params.key);
	  	}
	});	 

	return promise.promise;
}

exports.upload = upload;

exports.sendMail = function(params){
	var promise = promiseAdapter.defer();
	transporter.sendMail({
        from: 'no-reply@sivy.io',
        to: params.email,
        subject: params.subject,
        text: params.data
    },function(err,data) {
      if (err) {
       	promise.reject(err);
      } else {
      	promise.resolve(null);
      }
    });
	return promise.promise;
}


var downloadImage = function(params){
	var promise = promiseAdapter.defer();
	var link =  params.link.replace(" ", '');
	if (link.replace(" ", '') == "") {
		promise.reject("Wrong link")
	} else {
		var type = link.substr(0,5);
		var getter = http;

		if (type == "https") {
			getter = https;
		}
		console.log(link)
		getter.get(link, function(res){
			var imagedata = '';
	    res.setEncoding('binary');

	    res.on('data', function(chunk){
	    	imagedata += chunk;
	    });

	    res.on('error', function(err){
	    	promise.reject(err);
	    });

	    res.on('end', function(){
	    	fs.writeFile(
        	params.name, 
        	imagedata, 
        	'binary', 
        function(err){
        	if (err){
          	promise.reject(err)
          } else {
          	promise.resolve(null);
          }
        });
	    });
		})


	}
	
	return promise.promise;
}

function makeMarkerImage(params){
	var promise = promiseAdapter.defer(); 
	var self = this;
	graphic.cropImage(params.path_name)
  .then(function(){
    return graphic.resize({
      name: params.path_name,
      output_name: params.path_name,
      width: 110,
      height: 110
    })
  }, function(err){
  	throw err;
  }).then(function(name){
  	console.log('go on round =====')
    return graphic.roundImage({
      name: params.path_name,
      output_name: params.path_name
    })
  }, function(err){
  	throw err;
  }).then(function(name){
    return graphic.composite_image({
      name: params.path_name,
      output_name: "public/marker_" + params.name,
      marker: "public/marker/" + params.marker,
      position: "+9+8"
    }).then(function(name){
      upload({
      	name: "public/marker_" + params.name,
      	bucket: "sivy/marker",
      	key: params.name
      }).then(function(name){
      	promise.resolve(params.name)
      }, function(err){
	    	throw err;
	    })
    }, function(err){
    	throw err;
    });
  }, function(err){
  	throw err;
  })

  return promise.promise;
}

exports.getAvatar = function(params) {
	var promiseAd = promiseAdapter.defer();
	var avatarPath = "default-user-avatar.png";
	var promise;
	if (params.link == null || params.link.trim() == "" || params.link.indexOf("undefined") != -1) {
		promise = makeMarkerImage({
			name: avatarPath,
			marker: params.marker,
			path_name : "public/" + avatarPath
		})
	} else {
		params.link = params.link.split("?")[0];
		var name = makeid(10) + (new Date()).getTime() + ".png";
		var path_name = "public/" + name;
		promise = downloadImage({
			link : params.link,
			name: path_name
		}).then(function(){
			console.log('download complete')
			return makeMarkerImage({
				name: name,
				marker: params.marker,
				path_name: path_name
			})
		}, function(err){
			console.log(err)
		})
	}

	promise.then(function(name){
		fs.unlinkSync("public/marker_" + name);
		if (name != "default-user-avatar.png") {
			fs.unlinkSync("public/" + name);
		}
		promiseAd.resolve(name)
	}, function(err){
		promiseAd.reject(err);
	})

	return promiseAd.promise;
}