angular.module('frontpage', 
  [
    "googlechart",
    'ui.bootstrap.datetimepicker'
  ])
  .controller('indexController', [
    "$scope",
    "$http",
    "$interval",
    function($scope, $http, $interval) {
      $scope.start = new Date();
      $scope.start.setHours(0,0,0,0);
      $scope.start = $scope.start.getTime();
      //socket
      var socket = io("vo-phihung.me:6500");
      socket.on('new_data', function(msg) {
        $scope.$apply(function() {
          $scope.myChartObject.data.rows.push({
            "c": [{
              "v": ""
            }, {
              "v": msg.sensorValue
            }]
          });
          //limit to 50 object
          if ($scope.myChartObject.data.rows.length > 10) {
            $scope.myChartObject.data.rows.shift();
          }

          $scope.value1 = msg.sensorValue


          $scope.myChartObject2.data.rows.push({
            "c": [{
              "v": ""
            }, {
              "v": msg.sensorValue2
            }]
          });
          //limit to 50 object
          if ($scope.myChartObject2.data.rows.length > 10) {
            $scope.myChartObject2.data.rows.shift();
          }
          $scope.value2 = msg.sensorValue2       

          $scope.myChartObject3.data.rows.push({
            "c": [{
              "v": ""
            }, {
              "v": msg.sensorValue3
            }]
          });
          //limit to 50 object
          if ($scope.myChartObject3.data.rows.length > 10) {
            $scope.myChartObject3.data.rows.shift();
          }
          $scope.value3 = msg.sensorValue3     

          $scope.myChartObject4.data.rows.push({
            "c": [{
              "v": ""
            }, {
              "v": msg.sensorValue4
            }]
          });
          $scope.value4 = msg.sensorValue4
          //limit to 50 object
          if ($scope.myChartObject4.data.rows.length > 10) {
            $scope.myChartObject4.data.rows.shift();
          }     
          $scope.myChartObject5.data.rows.push({
            "c": [{
              "v": ""
            }, {
              "v": msg.sensorValue5
            }]
          });
          //limit to 50 object
          if ($scope.myChartObject5.data.rows.length > 10) {
            $scope.myChartObject5.data.rows.shift();
          }
          $scope.value5 = msg.sensorValue5   
          //limit 10 second
          //
          // console.log((new Date()).getTime() - $scope.myChartObject.data.rows[0].c[0].v)
          // if ($scope.myChartObject.data.rows.length > 0 && (((new Date()).getTime() - $scope.myChartObject.data.rows[0].c[0].v) > 10000)) { //or over 10 sec, shift
          //   $scope.myChartObject.data.rows.shift();
          // }
        })
      });
      //////////////////////////
      $scope.myChartObject = {
        "type": "LineChart",
        "displayed": false,
        "data": {
          "cols": [{
            "id": "month",
            "label": "Month",
            "type": "string",
            "p": {}
          }, {
            "id": "laptop-id",
            "label": "Sensor 1",
            "type": "number",
            "p": {}
          }],
          "rows": []
        },
        "options": {
          "title": "Data",
          "isStacked": "true",
          "fill": 20,
          "displayExactValues": true,
          "curveType": 'function',
          "vAxis": {
            "title": "Amount",
            "gridlines": {
              "count": 10
            }
          },
          "hAxis": {
            "title": "Date"
          }
        },
        "formatters": {}
      }

      $scope.myChartObject2 = {
        "type": "LineChart",
        "displayed": false,
        "data": {
          "cols": [{
            "id": "month",
            "label": "Month",
            "type": "string",
            "p": {}
          }, {
            "id": "laptop-id",
            "label": "Sensor 2",
            "type": "number",
            "p": {}
          }],
          "rows": []
        },
        "options": {
          "title": "Data",
          "isStacked": "true",
          "fill": 20,
          "displayExactValues": true,
          "curveType": 'function',
          "vAxis": {
            "title": "Amount",
            "gridlines": {
              "count": 10
            }
          },
          "hAxis": {
            "title": "Date"
          }
        },
        "formatters": {}
      }

      $scope.myChartObject3 = {
        "type": "LineChart",
        "displayed": false,
        "data": {
          "cols": [{
            "id": "month",
            "label": "Month",
            "type": "string",
            "p": {}
          }, {
            "id": "laptop-id",
            "label": "Sensor 2",
            "type": "number",
            "p": {}
          }],
          "rows": []
        },
        "options": {
          "title": "Data",
          "isStacked": "true",
          "fill": 20,
          "displayExactValues": true,
          "curveType": 'function',
          "vAxis": {
            "title": "Amount",
            "gridlines": {
              "count": 10
            }
          },
          "hAxis": {
            "title": "Date"
          }
        },
        "formatters": {}
      }

      $scope.myChartObject4 = {
        "type": "LineChart",
        "displayed": false,
        "data": {
          "cols": [{
            "id": "month",
            "label": "Month",
            "type": "string",
            "p": {}
          }, {
            "id": "laptop-id",
            "label": "Sensor 2",
            "type": "number",
            "p": {}
          }],
          "rows": []
        },
        "options": {
          "title": "Data",
          "isStacked": "true",
          "fill": 20,
          "displayExactValues": true,
          "curveType": 'function',
          "vAxis": {
            "title": "Amount",
            "gridlines": {
              "count": 10
            }
          },
          "hAxis": {
            "title": "Date"
          }
        },
        "formatters": {}
      }

      $scope.myChartObject5 = {
        "type": "LineChart",
        "displayed": false,
        "data": {
          "cols": [{
            "id": "month",
            "label": "Month",
            "type": "string",
            "p": {}
          }, {
            "id": "laptop-id",
            "label": "Sensor 2",
            "type": "number",
            "p": {}
          }],
          "rows": []
        },
        "options": {
          "title": "Data",
          "isStacked": "true",
          "fill": 20,
          "displayExactValues": true,
          "curveType": 'function',
          "vAxis": {
            "title": "Amount",
            "gridlines": {
              "count": 10
            }
          },
          "hAxis": {
            "title": "Date"
          }
        },
        "formatters": {}
      }

      $scope.currentTimelast = 0;
      $scope.startTimeInteval = (new Date()).getTime();
      $interval(function() {
        var currentTimestamp = (new Date()).getTime()
        if (currentTimestamp - $scope.startTimeInteval >=  60000) { //5 minutes 300000
          $scope.downloadFile($scope.startTimeInteval, currentTimestamp)
          $scope.startTimeInteval = currentTimestamp;
        }
      }, 1000)

      $scope.exportData = function() {
        var fromDate = (new Date($scope.dataFrom)).getTime();
        var toDate = (new Date($scope.dataTo)).getTime();
        $scope.downloadFile(fromDate,toDate)
        
      }

      $scope.downloadFile = function(from, to) {
        $http.post("/export", {
          from : $scope.start,
          to : to
        })
        .success(function(resp){

           var redirectWindow = window.open('/'+resp, '_blank');
          redirectWindow.location;
        })
      }
      $scope.isShowGraph = false;
      $scope.downloadFile(123, (new Date()).getTime())

      $scope.showGraph = function(){
        $scope.isShowGraph = !$scope.isShowGraph;
      }

      $scope.getDatabaseData = function() {
        $http.get('/getData')
        .then(function(resp){
          var data = resp.data;
          var i = data.length-1;
          while(i--) {
            $scope.myChartObject.data.rows.push({
              "c": [{
                "v": ""
              }, {
                "v": data[i].sensorValue
              }]
            });
            //limit to 50 object
            if ($scope.myChartObject.data.rows.length > 10) {
              $scope.myChartObject.data.rows.shift();
            }

            $scope.value1 = data[i].sensorValue


            $scope.myChartObject2.data.rows.push({
              "c": [{
                "v": ""
              }, {
                "v": data[i].sensorValue2
              }]
            });
            //limit to 50 object
            if ($scope.myChartObject2.data.rows.length > 10) {
              $scope.myChartObject2.data.rows.shift();
            }
            $scope.value2 = data[i].sensorValue2       

            $scope.myChartObject3.data.rows.push({
              "c": [{
                "v": ""
              }, {
                "v": data[i].sensorValue3
              }]
            });
            //limit to 50 object
            if ($scope.myChartObject3.data.rows.length > 10) {
              $scope.myChartObject3.data.rows.shift();
            }
            $scope.value3 = data[i].sensorValue3     

            $scope.myChartObject4.data.rows.push({
              "c": [{
                "v": ""
              }, {
                "v": data[i].sensorValue4
              }]
            });
            $scope.value4 = data[i].sensorValue4
            //limit to 50 object
            if ($scope.myChartObject4.data.rows.length > 10) {
              $scope.myChartObject4.data.rows.shift();
            }     
            $scope.myChartObject5.data.rows.push({
              "c": [{
                "v": ""
              }, {
                "v": data[i].sensorValue5
              }]
            });
            //limit to 50 object
            if ($scope.myChartObject5.data.rows.length > 10) {
              $scope.myChartObject5.data.rows.shift();
            }
            $scope.value5 = data[i].sensorValue5 
          }
        })
      }

      $scope.getDatabaseData();
    }
  ])